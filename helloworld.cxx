#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv) {
    
    char* inFileName = argv[1];
    char* inMessage  = argv[2];
    
    cout << "Hello World. We will  write a message to " << inFileName << endl;
    ofstream myFile;
    myFile.open( inFileName );
    myFile << "Writing this to a file. My message is " << inMessage << endl;
    myFile.close();
    
    return 0;
    
}
